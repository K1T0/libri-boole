# Libri-Boole

In questo repository viene  depositato una traduzione __AMATORIALE__ del libro
di George Boole "An Investigation On Laws of Thought", (investigazione sulle
leggi del pensiero). \
Il libro e' stato scritto in RMarkdown in modo da renderlo
facilmente editabile e forkabile da chiunque per la sintassi simile al
markdown, ma comunque soddisfacente all' occhio post traduzione in pdf. \
Viene dato sia il libro intero (sia in formato RMarkdown sia gia' convertito in
pdf), sia capitolo per capitolo. \
E' presente qualche nota mancante dalla versione
originale dispersa nei capitoli separati e non presente nella versione unita
per colpa di un bug nell' unificazione dei file.

# NOTA 

Siete pregati tutti di contattare il sottoscritto per informi su eventuali
correzioni e/o pubblicazioni di versioni stampate. 



