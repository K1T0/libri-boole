---
title:  Investigazione sulle leggi del pensiero (WIP)
author: George Boole, traduzione a cura di Daniele Sica
linkcolor: cyan
df_print: tibble
output_file: pdf_document
output: pdf_document
fontsize: 14pt
---

# CAPITOLO XVIII: ILLUSTRAZIONI ELEMENTARI DEL METODO GENERALE DELLE PROBABILITÀ.

1. In questa sede si intende illustrare, con esempi elementari, il metodo
   generale dimostrato nell'ultimo capitolo. Gli esempi scelti saranno
   principalmente quelli che, per la loro semplicità, consentono una pronta
   verifica delle soluzioni ottenute. Tuttavia, si farà cenno a una classe più
   elevata di problemi, che verranno in seguito considerati in modo più
   approfondito, la cui analisi sarebbe incompleta senza l'ausilio di un metodo
   distinto che determini le condizioni necessarie tra i loro dati, in modo che
   possano rappresentare un'esperienza possibile, e che assegni i limiti
   corrispondenti delle soluzioni finali. La considerazione più completa di
   questo metodo e delle sue applicazioni è riservata al prossimo capitolo.

2. ___Es. 1.___- La probabilità che tuoni in un dato giorno è $p$, la probabilità che
   tuoni e grandini è $q$, ma della connessione dei due fenomeni del tuono e
   della grandine non si suppone di sapere altro. Richieste le probabilità che
   grandini nel giorno proposto. \
   Sia $x$ a rappresentare l'evento: tuona \
   Che $y$ rappresenti l'evento: la grandine.\
   Allora $xy$ rappresenterà l'evento - tuona e grandina - e i dati del
   problema sono 
   \begin{center}
   Prob. $x = p$, Prob. $xy = q$. 
   \end{center}
   Essendo qui coinvolto un solo evento composto $xy$, assumiamo, secondo la
   regola, 
   \begin{center}
   $xy = u$ \\ (1). 
   \end{center}
   I nostri dati diventano quindi
   \begin{center}
   Prob.  $x = p$, Prob. $u = q$ \\ (2) 
   \end{center}
   ed è necessario trovare la Prob. $y$. Ora la (1) dà 
   \begin{center}
   $y = \frac{u}{x} = ux + \frac{1}{0}u(1 - x) + 0(1 - u)x + \frac{0}{0}(1 - u)(1 - x)$
   \end{center}
   Quindi (XVII. 17) troviamo 
   \begin{center}
   $V = ux + (1 - u)x + (1 - u)(1 - x)$ \\ 
   $V_x = ux + (1 - u)x = x$,   $V_u = ux$ \\
   \end{center}
   e le equazioni della Regola Generale, ossia,
   \begin{center}
   $\frac{V_x}{p} = \frac{V_u}{q} = V$\\
   Prob. $y =  \frac{A+cC}{V}$
   \end{center}
   diventano, per sostituzione, e osservando che $A = ux, C = (1 - u) (1 - x),$ e
   che $V$ si riduce a 
   \begin{center}
   $x + (1 - u) (1 - x)$, \\
   $\frac{x}{p} = \frac{ux}{q} = x + (1 - u)(1 - x)$ \\ (3) 
   Prob. $y = \frac{ux + c(1 - u)(1 - x)}{x + (1 - u)(1 - x)}$ \\ (4) 
   \end{center}
   da cui si deduce facilmente, attraverso l'eliminazione di $x$ e $u$,
   \begin{center}
   Prob. $y = q + c(1 - p)$ \\(5) 
   \end{center}
   In questo risultato $c$ rappresenta la probabilità sconosciuta che, se
   l'evento $(1 - u) (1 - x)$ si verifica, si verifichi anche l'evento $y$. Ora
   $(1- u) (1 - x) = (1 - xy) (1 - x) = 1 - x$, sulla moltiplicazione
   effettiva. Quindi c è la probabilità sconosciuta che, se non tuona,
   grandini.\
   La soluzione generale (5) può quindi essere interpretata come
   segue: -La probabilità che grandini è uguale alla probabilità che tuoni e
   grandini, $q$, insieme alla probabilità che non tuoni, $1 - p$, moltiplicata per
   la probabilità $c$, che se non tuona grandini. E il ragionamento comune
   verifica questo risultato.\
   Se $c$ non può essere determinato numericamente, assegnando ad esso i valori
   limite 0 e 1, si ottengono i seguenti limiti della Prob. $y$, ossia: 
   - Limite inferiore = $q$. 
   - Limite superiore = $q + 1 - p$.

3. __Es. 2.___ - La probabilità che uno o entrambi gli eventi si verifichino è p, che
   uno o entrambi falliscano è $q$. Qual è la probabilità che si verifichi solo
   uno di questi?\
   Se $x$ e $y$ rappresentano i rispettivi eventi, allora i dati
   sono
   \begin{center}
   Prob. $xy + x(1-y) + (1 -x)y = p$ \\
   Prob.$x(1 - y) + ( 1- x)y + (1 - x)(1 - y) = q$
   \end{center}
   e dobbiamo trovare
   \begin{center}
   Prob. $x (1 - y) + y (1 - x)$. 
   \end{center}
   In questo caso, essendo tutti gli eventi interessati composti, si assume
   \begin{center}
   $xy + x(1 - y) + (1 - x)y = s$\\
   $x(1 - y) + (1 - x)y + (1 - x)(1 - y) =t $\\
   $x(1 - y) + (1 - x)y = w$
   \end{center}
   Eliminando poi $x$ e $y$ e determinando $\omega$ come funzione evoluta di $s$ e
   $t$, troviamo 
   \begin{center}
   $w = st + 0s(1 - t) + 0(1 - s)t + \frac{1}{0}(1 - s)(1 - t)$
   \end{center}
   Quindi $A = st, C=0, V = st +s (1 - t) +(1 - s) t = s + (1 - s) t, V_s = s,
   V_t, = t;$ e le equazioni della regola generale (XVII. 17) diventano 
   \begin{center}
   $\frac{s}{p} = \frac{t}{q} = s(1 - s)t$ \\ (1) \\
   Prob. $w = \frac{st}{s + (1 - s)t}$
   \end{center}
   da cui troviamo, eliminando $s$ e $t$, 
   \begin{center}
   Prob. $\omega  = p + q - 1$. 
   \end{center}
   Quindi $p + q - 1$ è la misura della probabilità cercata. Questo risultato
   può essere verificato come segue: poiché $p$ è la probabilità che uno o
   entrambi gli eventi dati si verifichino, $1 - p$ sarà la probabilità che
   entrambi falliscano; e poiché $q$ è la probabilità che uno o entrambi
   falliscano, $1 - q$ è la probabilità che entrambi accadano. Quindi $1 - p +
   1 - q, o 2 - p - q,$ è la probabilità che si verifichino entrambi o che
   falliscano entrambi. Ma l'unica alternativa possibile è che uno solo degli
   eventi si verifichi, quindi la probabilità che si verifichi è $1 - (2 - p -
   q), o p + q - 1$, come sopra.

4. ___Es. 3.___- La probabilità che un testimone $A$ dica la verità è $p$, la probabilità
   che un altro testimone $B$ dica la verità è $q$, e la probabilità che non siano
   d'accordo in un'affermazione è $r$. Qual è la probabilità che, se sono
   d'accordo, la loro affermazione sia vera? \
   Che $x$ rappresenti l'ipotesi che $A$ dica la verità; $y$ che $B$ dica la verità;
   allora l'ipotesi che $A$ e $B$ siano in disaccordo nella loro affermazione sarà
   rappresentata da $x (1 - y) + y (1 - x)$; l'ipotesi che siano d'accordo
   nell'affermazione da $xy + (1 - x) (1 - y)$, e l'ipotesi che siano d'accordo
   nella verità da $xy$. \
   Abbiamo quindi i seguenti dati: \
   \begin{center}
   Prob. $x = p$, \\
   Prob. $y = q$, \\
   Prob. $x (1 - y) + y (1 - x) = r$, 
   \end{center}
   da cui dobbiamo determinare 
   \begin{center}
   $\frac{\text{Prob.}xy}{\text{Prob.}xy +(1-x)(1-y)}$
   \end{center}
   Ma poiché Prob. $x (1 - y) + y (1 - x) = r$, è evidente che Prob. $xy + (1 - x)
   (1 - y)$ sarà pari a $1 - r$; dobbiamo quindi cercare 
   \begin{center}
   $\frac{\text{Prob}xy}{1 -r}$
   \end{center}
   Essendo gli eventi composti in questione, $x (1 - y) + y (1 - x) e xy$,
   assumiamo che 
   \begin{center}
   $\left.
   \begin{array}{rr}
    x(1 - y) + y(1 - x) = s \\
    xy = w
    \end{array}
   \right\}.  $ \\
   (1) 
   \end{center}
   I nostri dati sono quindi Prob. $x = p$, Prob. $y = q$, Prob. $s = r$, e
   dobbiamo trovare la Prob. $\omega$ .\
   Il sistema (1) dà, in riduzione, 
   \begin{center}
   $\{x(1-y) + y(1-x)\}(1-s)+s\{xy+(1-x)(1-y)\}+xy(1-w)+w(1-xy)=0$
   \end{center}
   da cui
   \begin{center}
   $ w =  \frac{x( 1 -y)(1-s) + y(1-x)(1-s) + sxy+s(1-x)(1-y) + xy}{2xy - 1}$  \\
   $= \frac{1}{0}xys + xy(1 - s)+0x(1-y)s + \frac{1}{0}x(1-y)(1-s) +  0(1 - x)ys + \frac{1}{0}(1 - x)(1 - y)s + \frac{1}{0}(1 - x)y(1 - s) + 0(1 - x)(1 - y)(1 - s)$ \\
   (2) 
   \end{center}
   Nell'espressione di questo sviluppo, il coefficiente $\frac{1}{0}$ è stato fatto per
   sostituire ogni forma equivalente (X. 6). Qui abbiamo 
   \begin{center}
   $V = xy(1 - s) + x (1 - y) s + (1 - x) ys + (1 - x;) (1 - y) (1 - s)$;
   \end{center}
   Da qui, passando dalla Logica all'Algebra,
   \begin{center}
   $\frac{xy(1 - s) + x(1 - y)s}{p} = \frac{xy(1-s) + (1-x)ys}{q} = \frac{x(1 - y)s + (1-x)ys}{r}$ \\
   $ = xy(1 -s) +x(1-y)s + (1-x)ys + (1-x)(1-y)(1-s)$ \\
   Prob. $w = \frac{xy(1-s)}{xy(1-s) + x(1-y)s + (1-x)ys + (1-x)(1-y)(1-s)}$
   \end{center}
   da cui si deduce facilmente 
   \begin{center}
   Prob. $w = \frac{p + q - r}{2}$
   \end{center}
   da cui abbiamo 
   \begin{center}
   $\frac{\text{Prob.}xy}{1 - r} = \frac{p + q -r}{2(1 - r)}$ \\(3) 
   \end{center}
   per il valore ricercato. \
   Se allo stesso modo cerchiamo la probabilità che se $A$ e $B$ sono d'accordo
   nella loro affermazione, questa sarà falsa, dobbiamo sostituire la seconda
   equazione del sistema (1) con la seguente, cioè: $(1 - x) (1 - y) = x$;
   l'equazione logica finale sarà quindi 
   \begin{center}
   $w = \frac{1}{0}xys + 0xs(1 -s) + 0x(1-y)s + \frac{1}{0}x(1-y)(1-s) + 0(1-x)ys + \frac{1}{0}(1-x)y(1-s) + \frac{1}{0}(1-x)(1-y)s + (1-x)(1-y)(1-s)$ \\ (4) 
   \end{center}
   da cui, procedendo come in precedenza, si deduce infine 
   \begin{center}
   Prob. $\frac{2 - p - q -r}{2}$ \\(5) 
   \end{center}
   Perciò abbiamo
   \begin{center}
   $\frac{\text{Prob.}(1-x)(1-y)}{1 - r} = \frac{2 - p - q - r}{2(1 - r)}$ \\(6) 
   \end{center}
   per il valore qui ricercato. \
   Questi risultati sono reciprocamente coerenti. Infatti, poiché è
   certo che l'affermazione congiunta di $A$ e $B$ deve essere o vera o falsa, i
   secondi membri delle (3) e (5) dovrebbero, per addizione, fare 1. Ora
   abbiamo identico,
   \begin{center}
   $\frac{p+q-r}{2(1-r)} + \frac{2-p-q-r}{2(1-r) =1}$
   \end{center}
   È probabile, dalla semplicità dei risultati (5) e (6), che
   possano essere facilmente dedotti dall'applicazione di principi noti; ma va
   osservato che non rientrano direttamente nell'ambito dei _metodi_ noti. Il
   numero dei dati supera quello degli eventi semplici che coinvolgono. M.
   Cournot, nella sua opera molto valida "Exposition de la Theorie des
   Chances", ha proposto, in casi come quelli sopra descritti, di selezionare
   dalle premesse iniziali diversi insiemi di dati, ogni insieme pari al numero
   di eventi semplici che coinvolgono, di assumere che questi eventi semplici
   siano indipendenti, di determinare separatamente dai rispettivi insiemi di
   dati le loro probabilità e, confrontando i diversi valori così trovati per
   gli stessi elementi, di giudicare fino a che punto l'ipotesi di indipendenza
   sia giustificata. Questo metodo può avvicinarsi alla correttezza solo quando
   i suddetti eventi semplici si dimostrano, secondo il criterio di cui sopra,
   quasi o del tutto indipendenti; e nelle questioni di testimonianza e di
   giudizio, in cui si adotta tale ipotesi, sembra dubbio che essa sia
   giustificata dall'esperienza concreta delle vie degli uomini.

5. ___Es. 4.___- Da osservazioni fatte durante un periodo di malattia generale, c'era
   una probabilità $p$ che una casa presa a caso in un particolare distretto
   fosse visitata dalla febbre, una probabilità $q$ che fosse visitata dal
   colera, e una probabilità $r$ che sfuggisse a entrambe le malattie e non fosse
   in condizioni sanitarie difettose per quanto riguarda la pulizia e la
   ventilazione. Qual è la probabilità che una casa presa a caso sia in
   condizioni sanitarie difettose? \
   Con riferimento a una casa qualsiasi, appropriiamoci dei simboli $x, y, z$,
   come segue, viz: \  
   Il simbolo $x$ alla visita della febbre, $y$ quella del colera e $z$ che le
   condizioni sanitarie isano difettose. Gli eventi le cui probabilità sono
   date sono quindi denotati da $x, y e (1 - x) (1 - y) (1 - z),$ mentre
   l'evento di cui si cerca la probabilità è $z$. Si supponga quindi che, $(1 -
   x) (1 - y) (1 - z) = \omega$  ; allora i nostri dati sono, 
   - Prob. $x = p$,
   - Prob. $y = q$, 
   - Prob. $w = r$, 
   e dobbiamo trovare la Prob. $z$. \
   Ora 

   \begin{center}
   $z = \frac{(1 - x)(1 - y) - w}{(1 -x)(1 - y)}$ \\
   $= \frac{1}{0}zyw + \frac{0}{0}xy(1-w) + \frac{1}{0}x(1 - y)w + \frac{0}{0}x(1 - y)(1 -w) + \frac{1}{0}(1 - x)yw + \frac{0}{0}(1 - x)y(1 - w) + 0(1 - x)(1 - y)w$ \\
   (1)
   \end{center}
   Il valore di $V$ dedotto da quanto detto sopra è
   \begin{center}
   $V =  xy(1 -w)+ x(1 - y)(1 -w) + (1-x)y(1 -w) + (1 -x)(1 - y)w + (1 -x)(1 - y)(1 -w) = 1 -w + w(1-x)(1-y)$
   \end{center}
   e riducendo analogamente $V_x V_y, V_w$ , otteniamo 
   \begin{center}
   $V_x = x (1 - \omega ), V_y = y (1 - \omega), V_\omega = \omega(1 - x) (1- y);$ 
   \end{center}
   fornendo le equazioni algebriche
   \begin{center}
   $\frac{x(1-w)}{p} = \frac{y(1-w)}{q} = \frac{w(1-x)(1-y)}{r} = 1-w + w(1-x)(1-y)$ \\(2) 
   \end{center}
   Per quanto riguarda i termini dello sviluppo caratterizzati dai coefficienti
   $\frac{0}{0}$, invece di raccoglierli in un unico termine, li presenterò, per amore di
   varietà (XVII. 18), nella forma 
   \begin{center}
   $\frac{0}{0}x(1-w) + \frac{0}{0}(1-x)y(1-w)$ \\(3) 
   \end{center}
   il valore della Prob. $z$ sarà quindi 
   \begin{center}
   Prob.$z = \frac{(1-x)(1-y)(1-w)+cx(1-w)+c'(1-x)y(1-w)}{1-w +w(1-x)(1-y)}$ \\(4)
   \end{center}
   Da (2) e (4) si deduce
   \begin{center}
   Prob.$z = \frac{(1 - p -r)(1 - q - r)}{1 - r} + cp +c'\frac{q(1 - p - r)}{1 - r}$
   \end{center}
   che come espressione della probabilità richiesta. Se in questo risultato
   facciamo $c = 0, e c' = 0$, troviamo per un limite inferiore del suo valore
   $\frac{(1 - p - r)(1 - q -r)}{1- r}$, e se facciamo $c = 1, c'= 1$,
   otteniamo per il suo limite superiore $1 - r$.

6. Dall'esame di questa soluzione risulta che le premesse scelte erano
   estremamente difettose. Le costanti $c$ e $c'$ lo indicano, e i termini
   corrispondenti (3) dell'equazione logica finale mostrano come si debba
   colmare la lacuna. Quindi, poiché 
   \begin{center}
   $x( 1 -w) = x\{1 - (1 -x)(1 - y)(1 - z)\} = x$ \\
   $(1 - x)y(1 - w) = (1 -x)y\{1 - (1  - x)(1 -y)(1 -z)\} = (1 -x)y$
   \end{center}
   impariamo che $c$ è la probabilità che se una casa è stata visitata dalla
   febbre le sue condizioni igieniche siano difettose, e che $c'$ è la
   probabilità che se una casa è stata visitata dal colera senza febbre, le sue
   condizioni igieniche siano difettose.\
   Se i termini dello sviluppo logico interessati dal coefficiente $\frac{0}{0}$ fossero
   stati raccolti insieme come nell'enunciato diretto della regola generale, la
   soluzione finale avrebbe assunto la seguente forma: 
   \begin{center}
   Prob.$z = \frac{(1 - p -r)(1 - q- r)}{1 - r} + c(p+q - \frac{pq}{1-r})$
   \end{center}
   $c$ rappresenta la probabilità che, se una casa è stata visitata da una o da
   entrambe le malattie menzionate, le sue condizioni igienico-sanitarie erano
   difettose. Questo risultato è perfettamente coerente con il precedente, e in
   effetti si può stabilire formalmente la _necessaria_ equivalenza delle diverse
   forme di soluzione presentate in questi casi. \
   La soluzione di cui sopra può essere verificata in casi particolari. Così,
   prendendo la seconda forma, se $c = 1$ troviamo Prob. $z = 1 - r$, un risultato
   corretto. Infatti, se la presenza di febbre o colera indica _certamente_ una
   condizione sanitaria difettosa, la probabilità che una casa si trovi in uno
   stato sanitario difettoso sarebbe semplicemente uguale alla probabilità di
   _non_ trovarla in quella categoria indicata con $z$, la cui probabilità, in base
   ai dati, sarebbe $1 - r$. Forse la verifica generale della soluzione di cui
   sopra sarebbe difficile.\
   Le costanti $p, q e r$ nella soluzione di cui sopra sono soggette alle
   condizioni
   \begin{center}
   $p + r \leq 1$, $q + r \leq 1$
   \end{center}
  
7. ___Es. 5.___- Date le probabilità delle premesse di un sillogismo ipotetico,
   trovare la probabilità della conclusione. \
   Sia il sillogismo nella sua forma nuda e cruda come segue : \
   __Premessa maggiore:__ se la proposizione $Y$ è vera, $X$ è vera.\
   __Premessa minore:__ Se la proposizione $Z$ è vera, $Y$ è vera.\
   __Conclusione:__ Se la proposizione $Z$ è vera, $X$ è vera. \
   Supponiamo che la probabilità della premessa maggiore sia $p$, quella
   della premessa minore $q$. \
   I dati sono quindi i seguenti, rappresentando la proposizione $X$ con $x$, ecc.,
   e assumendo $c$ e $c'$ come costanti arbitrarie: 
   \begin{center}
   Prob.$y = c$,   Prob.$xy = cp$ \\
   Prob.$z = c'$,   Prob.$yz = c'q$\\
   \end{center}
   da cui dobbiamo determinare,
   \begin{center}
   $\frac{\text{Prob.}xz}{\text{Prob.}z}$ oppure $\frac{\text{Prob.}xz}{c'}$
   \end{center}
   Supponiamo che,
   \begin{center}
   $xy = u, yz = \upsilon, xz = \omega$; 
   \end{center}
   allora, procedendo secondo il metodo consueto per determinare $\omega$ come
   funzione evoluta di $y, z, u e v,$ i simboli corrispondenti alle proposizioni
   le cui probabilità sono date, troviamo
   \begin{center}
   $w = uz\upsilon y+ 0u(1-z)(1-\upsilon)y + 0(1-u)z\upsilon y + \frac{0}{0}(1-u)z(1-\upsilon)(1-y) + 0(1-u)(1-z)(1-\upsilon)y + 0(1-u)(1-z)(1-\upsilon)(1-y) +  $ termini con coefficienti uguali a $\frac{1}{0}$
   \end{center}
   e passando dalla Logica all'Algebra,
   \begin{center}
   $\frac{uz\upsilon y+ u(1-z)(1-\upsilon)y}{zp} = \frac{uz\upsilon y + (1-u)z\upsilon y + (1-u)z(1-\upsilon)(1-y)}{c'}$\\
   $= \frac{uz\upsilon y + (1-u)z\upsilon y}{c'q}$\\
   $\frac{uz\upsilon y + u(1-z)(1-\upsilon)y + (1-u)z\upsilon y + (1-u)(1-z)(1 - \upsilon)y}{c} = V$ \\
   Prob.$w = \frac{uz\upsilon y + a(1-u)z(1-\upsilon)(1-y)}{V}$
   \end{center}
   dove
   \begin{center}
   $V = uz\upsilon y + u(1-z)(1-\upsilon)y + (1-u)z\upsilon y + (1-u)z(1-\upsilon)(1-y) + (1-u)(1-z)(1-\upsilon)y +(1-u)(1-z)(1-\upsilon)(1-y) $
   \end{center}
   la soluzione di questo sistema di equazioni dà 
   \begin{center}
   Prob. $\omega  = c'pq + ac' (1 - q),$
   \end{center}
   da cui 
   \begin{center}
   $\frac{\text{Prob.}xy}{c'} = pq + a(1-q)$
   \end{center}
   il valore richiesto. In questa espressione la costante arbitraria $a$ è la
   probabilità che se la proposizione $Z$ è vera e $Y$ falsa, $X$ è vera. In altre
   parole, è la probabilità che se la premessa minore è falsa, la conclusione è
   vera. \
   Questa indagine avrebbe potuto essere notevolmente semplificata ipotizzando
   che la proposizione $Z$ sia vera e cercando quindi la probabilità di $X$. I
   dati sarebbero stati semplicemente 
   \begin{center}
   Prob.$ y = q$, Prob. $xy = pq$ ; 
   \end{center}
   da cui avremmo dovuto trovare Prob.$x = pq + a (1 - q)$. È evidente che, date
   le circostanze, questo modo di procedere sarebbe stato ammissibile, ma ho
   preferito dedurre la soluzione con l'applicazione diretta e incondizionata
   del metodo. Il risultato è quello che il ragionamento ordinario verifica e
   che non richiede un calcolo per essere ottenuto. I metodi generali tendono
   ad apparire più macchinosi quando vengono applicati a casi in cui il loro
   aiuto è meno richiesto. \
   Va osservato che il metodo sopra descritto è ugualmente applicabile al
   sillogismo categorico, e non solo al sillogismo, ma a ogni forma di
   ragionamento deduttivo. Date le probabilità che si attribuiscono
   separatamente alle premesse di un qualsiasi ragionamento, è sempre possibile
   determinare, con il metodo sopra descritto, la conseguente probabilità di
   verità di una conclusione legittimamente tratta da tali premesse. Non è
   necessario ricordare al lettore che la verità e la correttezza di una
   conclusione sono cose diverse.

8. Una circostanza notevole che si presenta in queste applicazioni merita di
   essere notata in modo particolare. Si tratta del fatto che le proposizioni
   che, quando sono vere, sono equivalenti, non lo sono necessariamente se
   considerate solo come probabili. Questo principio sarà illustrato nel
   seguente esempio. \
   ___Es. 6.___- Data la probabilità $p$ della proposizione
   disgiuntiva "O la proposizione $Y$ è vera, o entrambe le proposizioni $X$ e $Y$
   sono false", si richiede la probabilità della proposizione condizionale "Se
   la proposizione $X$ è vera, $Y$ è vera". \
   Siano $x$ e $y$ appropriati alle proposizioni $X$ e $Y$ rispettivamente. Allora si
   ha 
   \begin{center}
   Prob.$y + (1 - x) (1 - y) = p,$
   \end{center}
   da cui è necessario trovare il valore di $\frac{\text{Prob.}xy}{\text{Prob.}x}$. \
   Assumere 
   \begin{center}
   $y + (1 - x)(1 - y) = t$ \\(1) 
   \end{center}
   Eliminando $y$ si ottiene 
   \begin{center}
   $(1 - x) (1 - t) = 0.$ 
   \end{center}
   da cui
   \begin{center}
   $x = \frac{0}{0}t + 1 -t$
   \end{center}
   e procedendo nel modo consueto, 
   \begin{center}
   Prob.$x = 1 - p + cp$ \\(2) 
   \end{center}
   Dove $c$ è la probabilità che se $Y$ è vero, o $X$ e $Y$ falsi, $X$ è vero. \
   Troviamo quindi la Prob.$xy$. Si supponga 
   \begin{center}
   $xy = w$ \\ (3) 
   \end{center}
   Eliminando $y$ da (1) e (3) si ottiene 
   \begin{center}
   $z (1 - t) = 0 ;$
   \end{center}
   Da qui, procedendo come sopra, 
   \begin{center}
   Prob.$z = cp$, 
   \end{center}
   $c$ con la stessa interpretazione di prima. Quindi 
   \begin{center}
   $\frac{\text{Prob.}xy}{\text{Prob.}x} = \frac{cp}{1 - p +cp}$
   \end{center}
   per la probabilità di verità della proposizione condizionale data. \ Ora,
   nella scienza della Logica pura, che, in quanto tale, si occupa solo di
   verità e di falsità, le proposizioni disgiuntive e condizionali di cui sopra
   sono equivalenti. Sono vere e false insieme. Si vede, tuttavia,
   dall'indagine precedente, che quando la proposizione disgiuntiva ha una
   probabilità $p$, la proposizione condizionale ha una probabilità diversa e
   in parte indefinita $\frac{cp}{1-p+cp}$.Tuttavia queste espressioni sono
   tali che _quando una di esse diventa 1 o 0, l'altra assume lo stesso
   valore_. I risultati sono quindi perfettamente coerenti e la trasformazione
   logica serve a verificare la formula dedotta dalla teoria delle probabilità.\
   Il lettore potrà facilmente dimostrare, con un'analisi analoga, che se la
   probabilità della proposizione condizionale fosse data come $p$, quella della
   proposizione disgiuntiva sarebbe $1 - c + cp$, dove $c$ è la probabilità
   arbitraria della verità della proposizione $X$.

9. ___Es. 7.___- Si chiede di determinare la probabilità di un evento x, avendo dato
   il primo, o il primo e il secondo, o il primo, il secondo e il terzo dei
   seguenti dati, ossia: \
   1) La probabilità che l'evento $x$ si verifichi, o che uno solo dei tre eventi
   $x, y, z,$ fallisca, è $p$. 
   2) La probabilità che l'evento $y$ si verifichi, o che solo questo dei tre
   eventi $x, y, z,$ fallisca, è $q$. 
   3) La probabilità che l'evento $z$ si verifichi, o che solo questo dei tre
   eventi $x, y, z,$ fallisca, è $r$.

## SOLUZIONE DEL PRIMO CASO.

Supponiamo che sia fornito solo il primo dei dati sopra citati. \
Abbiamo quindi, 
\begin{center}
Prob.$\{x + (1 - x) yz\} = p$,
\end{center}
per trovare la Prob. $x$.\
Lasciate che 
\begin{center}
$x + (1 - x) yz = s,$
\end{center}
eliminando $yz$ come simbolo singolo, si ottiene, 
\begin{center}
$x (1 - s) = 0.$
\end{center}
Quindi 
\begin{center}
$x = \frac{0}{1 - s}= \frac{0}{0}s + 0(1-s)$
\end{center}
Da qui, procedendo secondo la regola, abbiamo 
\begin{center}
Prob.$x = cp$ \\
(1)
\end{center}
dove $c$ è la probabilità che se $x$ si verifica, o da solo fallisce, la prima
delle due alternative sia quella che si verificherà. I limiti della soluzione
sono evidentemente 0 e $p$. \
Questa soluzione sembra non darci alcuna informazione al di là di quella che il
buon senso avrebbe trasmesso senza l'aiuto di nessuno. Tuttavia, è tutto ciò
che il singolo dato assunto ci permette di dedurre. Nella prossima soluzione
vedremo come un'aggiunta ai nostri dati restringa entro limiti più ristretti la
soluzione finale.

## SOLUZIONE DEL SECONDO CASO.

Assumiamo come dati le equazioni 
\begin{center}
Prob.$\{x + (1-x)yz\} = p$\\
Prob.$\{y + (1-y)xz\} = q$
\end{center}
Scriviamo 
\begin{center}
$x + (1-x)yz =s$\\
$y + (1 -y)xz =t$
\end{center}
dalla prima delle quali abbiamo, per (VIII. 7), 
\begin{center}
$\{x + (1 - x) yz\} (1 - s) + s \{1 - x - (1 - x) yz\} =0$,
\end{center}
o 
\begin{center}
$(x + \bar{x}yz)\bar{s} + s\bar{x}(1-yz) = 0$
\end{center}
a condizione che per semplicità si scriva $\bar{x}$ per $1 - x$, $\bar{y}$ per
$1 - y$, e così via. Ora, scrivendo per $1 - yz$ il suo valore in costituenti,
abbiamo 
\begin{center}
$(x + \bar{x}yz)\bar{s} + s\bar{x}(y\bar{z} + \bar{y}z + \bar{y}\bar{z}) = 0$
\end{center}
un'equazione composta esclusivamente da termini positivi. \
In modo analogo si ha dalla seconda equazione,
\begin{center}
$(y + \bar{y}xz)\bar{t} + t\bar{y}(x\bar{z} + \bar{x}z + \bar{x}\bar{z}) = 0$
\end{center}
e dalla somma di queste due equazioni dobbiamo eliminare $y$ e $z$. \
Se in questa somma facciamo $y = 1, z = 1$, otteniamo il risultato $\bar{s} + \bar{t}$\
Se nella stessa somma facciamo $y = 1, z = 0$, otteniamo il risultato $x\bar{s} + s\bar{x} +\bar{t}$ \
Se nella stessa somma facciamo $y = 0, z = 1$, otterremo $x\bar{s} + s\bar{x} + x\bar{t}+t\bar{x}$\
E se, infine, nella stessa somma facciamo $y = 0, z = 0$, troviamo $x\bar{s} + s\bar{x} +tx + t\bar{x}$ o $x\bar{s} + s\bar{x} +t$ \
Queste quattro espressioni vanno moltiplicate insieme. La prima e la terza
possono essere moltiplicate nel modo seguente: 
\begin{center}
$(\bar{s} + \bar{t}(x\bar{s} + s\bar{x} + x\bar{t} +t\bar{x}))$ \\ 
$=x\bar{s} + x\bar{t} + (\bar{s} +\bar{t})(s\bar{x} + t\bar{x})$ per (IX. Prop II) \\
$=x\bar{s} + x\bar{t} +\bar{x}\bar{s}t + s\bar{x}\bar{t}$ \\(2) 
\end{center}
Ancora, il secondo e il quarto danno (IX. Prop. 1.) 
\begin{center}
$(x\bar{s} + s\bar{x} +\bar{t})(x\bar{s} + s\bar{x} + t) = x\bar{s} + s\bar{x}$ \\(3) 
\end{center}
Infine, (2) e (3) moltiplicati insieme danno 
\begin{center}
$(x\bar{s} + s\bar{x})(x\bar{s}+s\bar{x}\bar{t} + x\bar{t} + t\bar{x}\bar{s}) = x\bar{s} + s\bar{x}(s\bar{x}\bar{t}+x\bar{t}+t\bar{x}\bar{s}) = x\bar{s} + s\bar{x}\bar{t}$
\end{center}
L'equazione finale è quindi 
\begin{center}
$(1 - s) x + s (1 - t) (1 - x) = 0,$
\end{center}
che, risolto con riferimento a $x$, dà 
\begin{center}
$x = \frac{s(1-t)}{s(1-t) - (1-s)}$ \\
$= \frac{0}{0}st +s(1-t) + 0(1-s)t + 0(1-s)(1-t)$
\end{center}
e, procedendo secondo la regola, abbiamo infine, 
\begin{center}
Prob.$x = p(1-q)+cpq$ \\(4) 
\end{center}
dove $c$ è la probabilità che se l'evento $st$ si verifica, $x$ si verificherà.
Ora, se formiamo l'espressione sviluppata di st moltiplicando insieme le
espressioni per $s$ e $t$, troviamo $c =$ Probabilità che se $x$ e $y$ accadono insieme,
o $x$ e $z$ accadono insieme, e $y$ fallisce, o $y$ e $z$ accadono insieme, e $x$ fallisce,
l'evento $x$ accadrà. \
I limiti della Prob. $x$ sono evidentemente $p (1 - q)$ e $p$.\
Questa soluzione è più definita della precedente, in quanto contiene un termine
non influenzato da una costante arbitraria.

## SOLUZIONE DEL TERZO CASO.

Qui i dati sono
\begin{center}
Prob.$\{x +(1-x)yz\} = p$ \\
Prob.$\{y +(1-y)xz\} = q$ \\
Prob.$\{z +(1-z)yx\} = r$ \\
\end{center}
Scriviamo $\bar{x}$, come prima, per $1 - x$, ecc., e assumiamo 
\begin{center}
$x + \bar{x}yz =s$\\
$y + \bar{y}xz = t$ \\
$z + \bar{z}xy = u$
\end{center}
Riducendo per (VIII. 8) si ottiene l'equazione 
\begin{center}
$(x + \bar{x}yz)\bar{s} + s\bar{x}(y\bar{z} + \bar{y}z + \bar{y}\bar{z}) + (y + \bar{y}xz)\bar{t} + t\bar{y}(z\bar{x} + x\bar{z} + \bar{x}\bar{z}) + (z +\bar{z}xy)\bar{u} + u\bar{z}(x\bar{y} + \bar{x}y + \bar{x}\bar{y}) = 0$ \\
(5)
\end{center}
Ora, invece di eliminare direttamente $y$ e $z$ dall'equazione precedente,
assumiamo, in accordo con (IX. Prop. III.), che il risultato di tale
eliminazione sia 
\begin{center}
$Ex + E' ( 1 - x) = 0,$
\end{center}
allora $E$ si troverà facendo nell'equazione data $x = 1$, ed eliminando $y$ e $z$
dall'equazione risultante, ed $E'$ si troverà facendo nell'equazione data $x = 0$,
ed eliminando $y$ e $z$ al risultato. Per prima cosa, quindi, facendo $x = 1$, si ha
\begin{center}
$\bar{s} + (y +\bar{y}z)\bar{t} + t\bar{y}\bar{z} + (z +y\bar{z})\bar{u} + u\bar{y}\bar{z} = 0$
\end{center}
e facendo nel primo membro di questa equazione successivamente $y = 1, z = 1, y
= 1, z = 0,$ ecc., e moltiplicando insieme i risultati, si ha l'espressione
\begin{center}
$(\bar{s} + \bar{t} + \bar{u})(\bar{s} + \bar{t} + \bar{u})(\bar{s} + \bar{t} + \bar{u})(\bar{s} + t + u)$
\end{center}
che è equivalente a 
\begin{center}
$(\bar{s} + \bar{t} + \bar{u})(\bar{s} + t + u)$
\end{center}
Questa è l'espressione per $E$. La manterremo nella sua forma attuale. È già
stato dimostrato con un esempio (VIII. 3) che l'effettiva riduzione di tali
espressioni mediante moltiplicazione, sebbene conveniente, non è necessaria. \
Sempre nella (5), facendo $x = 0$, si ha
\begin{center}
$yz\bar{s} + s(y\bar{z} +\bar{y}z + \bar{y}\bar{z}) + y\bar{t} + t\bar{y} + z\bar{u}+ u\bar{z} = 0$
\end{center}
da cui, con lo stesso processo di eliminazione, troviamo per $E'$ l'espressione
\begin{center}
$(\bar{s} + \bar{t}  +\bar{u})(s + \bar{t} + u)(s + t+ \bar{u})(s +t +u)$
\end{center}
Il risultato finale dell'eliminazione di $y$ e $z$ dalla (5) è quindi 
\begin{center}
$(\bar{s} + \bar{t}  +\bar{u})(\bar{s} +  t + u)x + (\bar{s} + \bar{t}  +\bar{u})(s +\bar{t} + u)(s + t +\bar{u})(s + t +u)(1-x) = 0$
\end{center}
Da qui abbiamo
\begin{center}
$x = \frac{(\bar{s} + \bar{t}  +\bar{u})(s + \bar{t} + u)(s + t+ \bar{u})(s +t +u)}{(\bar{s} + \bar{t}  +\bar{u})(s + \bar{t} + u)(s + t+ \bar{u})(s +t +u) - (\bar{s} + \bar{t}  +\bar{u})(\bar{s} + t + u)}$
\end{center}
oppure, sviluppando il secondo membro, 
\begin{center}
$x = \frac{0}{0}stu + \frac{1}{0}s\bar{t}u +\frac{1}{0}st\bar{u} + s\bar{t}\bar{u} + \frac{1}{0}\bar{s}tu + 0\bar{s}\bar{t}u + 0\bar{s}t\bar{u} + 0\bar{s}\bar{t}\bar{u}$ \\(6) 
\end{center}
Da qui il passaggio dalla Logica all'Algebra,
\begin{center}
$\frac{stu +s\bar{t}\bar{u}}{p} = \frac{stu +\bar{s}t\bar{u}}{q} =\frac{stu + \bar{s}\bar{t}u}{r} = stu + s\bar{t}{u} + \bar{s}\bar{t}u + \bar{s}t\bar{u}+\bar{s}\bar{t}\bar{u}$ \\
(7) \\
Prob.$x = \frac{s\bar{t}\bar{u} + cstu}{stu + s\bar{t}\bar{u} + \bar{s}\bar{t}u  + \bar{s}t\bar{u} + \bar{s}\bar{t}\bar{u}}$
(8) \\
\end{center}
Per semplificare questo sistema di equazioni, cambiare $\frac{s}{\bar{s}}$ in
$s$, $\frac{t}{\bar{t}}$ in $t$, ecc., e dopo la modifica lasciare che
$\lambda$ stia per $stu + s + t + 1$. Si ha quindi
\begin{center}
Prob.$x = \frac{s + cstu}{\lambda}$ \\
(9) 
\end{center}
con le relazioni 
\begin{center}
$\frac{stu +s}{p} = \frac{stu +t}{q} = \frac{stu + u}{r} = stu +s +t +u +1 = \lambda$ \\
(10)
\end{center}
Da queste equazioni si ottiene 
\begin{center}
$stu + s = \lambda p$ \\
$stu + s = \lambda - t - u -1$ \\
$\cdot\dot{}\cdot \lambda p = \lambda - u -t -1 $\\
$u + t = \lambda (1 -p) -1 $\\
(11) 
\end{center}
Allo stesso modo,
\begin{center}
$u + s = \lambda (1 - q) - 1$, 
\end{center}
e 
\begin{center}
$s + t = \lambda (1 - r) - 1$
\end{center}
Da queste equazioni troviamo 
\begin{center}
$s = \frac{\lambda (1 + p - q - r) - 1}{2}$,   $t =\frac{\lambda (1 + q -r -p)-1}{2}$ \\
$u = \frac{\lambda (1 +r -p -q) - 1}{2}$ \\
(12) 
\end{center}
Ora, in base alla (10), 
\begin{center}
$stu = \lambda p - s.$
\end{center}
Sostituendo in questa equazione i valori di $s, t$ e $u$ determinati in
precedenza, si ha 
\begin{center}
$\{(1 + p -q - r)\lambda - 1\}\{(1 + q - p-r)\lambda -1\}\{(1+r-q-p)\lambda -1\} = 4\{(p + q +r -1)\lambda +1\}$ \\
(13) 
\end{center}
Un'equazione che determina $\lambda$. I valori di $s, t$ e $u$ sono quindi dati
dalla (12) e la loro sostituzione nella (9) completa la soluzione del problema.

10. Si pone ora una difficoltà, la cui soluzione è stata oggetto di questa
   indagine. Come si può determinare quale radice dell'equazione di cui sopra
   debba essere assunta per il valore di $\lambda$. A questa difficoltà si è
   fatto riferimento nell'apertura del presente capitolo e si è detto che il
   suo esame più approfondito era riservato al prossimo; da questo capitolo
   sono tratti i seguenti risultati. \
   Affinché i dati del problema possano essere ricavati da una possibile
   esperienza, le quantità $p, q$ e $r$ devono essere soggette alle seguenti
   condizioni: 

   \begin{center}
   $1 + p -q - r \geq 0$ \\
   $1 + q - p -r \geq 0$\\
   $1 + r -p -q \geq 0$ \\
   (14) 
   \end{center}
   Inoltre, il valore di $\lambda$ da impiegare nella soluzione generale deve
   soddisfare le seguenti condizioni :
   \begin{center}
   $\lambda \geq  \frac{1}{1 + p -q -r}$ , $\lambda \geq  \frac{1}{1 + q - p -r}$,  $\lambda \geq  \frac{1}{1 + r -q -p}$ \\
   (15) 
   \end{center}
   Queste due serie di condizioni sono sufficienti per limitare la soluzione
   generale. Si può dimostrare che l'equazione centrale (13) fornisce un solo
   valore di $\lambda$ che soddisfa queste condizioni, e questo valore di
   $\lambda$ è quello richiesto. Sia $1 + p - q - r$ il minore dei tre
   coefficienti di $\lambda$ dati sopra, allora $\frac{1}{1 + p -q -r}$ sarà il maggiore di questi
   valori, al di sopra del quale dovremo dimostrare che esiste un solo valore
   di $\lambda$. Scriviamo la (13) nella forma 
   \begin{center}
   $\{(1 + p -q -r)\lambda -1\}\{(1 + q -p -r)\lambda -1\} \{(1 + r -q -p)\lambda -1\} - 4\{(p + q+r-1)\lambda +1\} = 0$\\
   (16)
   \end{center}
   e rappresentare il primo membro con $V$. \
   Assumiamo $\lambda = \frac{1}{1 + p - q -r}$ , allora $V$ diventa che è
   _negativo_. \
   Sia $\lambda = \infty$, allora $V$ è _positivo e infinito_. \
   Di nuovo, 
   \begin{center}
   $\frac{d^2\lambda}{d\lambda^2} = (1 +p-q-r)(1+q-p-r)\{(1+r-p-q)\lambda -1\} + $ termini positivii simili
   \end{center}
   la cui espressione è positiva tra i limiti $\lambda = \frac{1}{1 + p-q-r}$ e
   $\lambda$ = $\infty$. \ Se poi costruiamo una curva la cui ascissa sarà
   misurata da $\lambda$ e le cui ordinate da $V$, questa curva, tra i limiti
   indicati, passerà da sotto a sopra l'ascissa $\lambda$, essendo la sua
   convessità sempre verso il basso. Quindi intersecherà una sola volta
   l'ascissa $\lambda$ entro quei limiti; e l'equazione (16) avrà quindi una
   sola radice corrispondente. La soluzione è quindi espressa dalla (9),
   essendo $\lambda$ la radice della (13) che soddisfa le condizioni (15) e
   essendo $s, t$ e $u$ date dalla (12). L'interpretazione di $c$ può essere dedotta
   nel modo consueto. \
   Da quanto detto sopra, risulta che il problema è, in tutti i casi, più o
   meno indeterminato.
